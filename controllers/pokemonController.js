const { mypokemon } = require("../models");
module.exports = {
  index: (req, res) => {
    res.status(200).json({
      message: "Selamat datang di Pokemon API!",
      listRoute: [
        {
          catch: "/api/catch",
          method: "post",
        },
        {
          release: "/api/release/:id",
          method: "get",
        },
        {
          rename: "/api/rename/:id",
          method: "put",
        },
        {
          getAll: "/api/mypokemon",
          method: "get",
        },
        {
          getById: "/api/mypokemon/:id",
          method: "get",
        },
        {
          delete: "/api/mypokemon/:id",
          method: "delete",
        },
      ],
    });
  },

  getPokemon: async (req, res) => {
    try {
      const data = await mypokemon.findAll();
      return res.status(200).json({ data });
    } catch (error) {
      return res.status(500).json({ error: error.message });
    }
  },

  getPokemonId: async (req, res) => {
    try {
      const { id } = req.params;
      const pokemon = await mypokemon.findOne({
        where: { id: id },
      });
      if (pokemon) {
        return res.status(200).json({ pokemon });
      }
      return res
        .status(404)
        .send("Pokemon with the specified ID does not exists");
    } catch (error) {
      return res.status(500).send(error.message);
    }
  },

  deletePokemon: async (req, res) => {
    try {
      const { id } = req.params;
      const deleted = await mypokemon.destroy({
        where: { id: id },
      });
      if (deleted) {
        return res.status(204).send("pokemon deleted");
      }
      throw new Error("pokemon not found");
    } catch (error) {
      return res.status(500).send(error.message);
    }
  },

  catchPokemon: async (req, res) => {
    let luck = Math.floor(Math.random() * 2);
    if (luck == 0) {
      res.status(200).json({
        message: "You failed catched a pokemon",
      });
    } else {
      try {
        const name = req.body.pokemon_name;
        const newPokemon = await mypokemon.create({
          pokemon_name: name,
          nickname: name,
          initial_release: 0,
          release: 0,
          release_before: 0,
          is_rename: false,
        });
        return res.status(201).json({
          message: "You catched a pokemon",
          data: newPokemon,
        });
      } catch (error) {
        return res.status(500).json({ error: error.message });
      }
    }
  },

  rename: async (req, res) => {
    try {
      const { id } = req.params;
      const [updated] = await mypokemon.update(req.body, {
        where: { id: id },
      });
      if (updated) {
        const updatedNickname = await mypokemon.findOne({ where: { id: id } });
        return res.status(200).json({ data: updatedNickname });
      }
      throw new Error("Pokemon not found");
    } catch (error) {
      return res.status(500).send(error.message);
    }
  },

  release: async (req, res) => {
    let luck = Math.floor(Math.random() * 99);
    const checkPrime = (num) => {
      for (let i = 2; i < num; i++) {
        if (num % i == 0) {
          return false;
        }
      }
      return true;
    };

    const prime = checkPrime(luck);
    if (luck <= 1) {
      res.status(200).json({
        message: "Failed to relase pokemon",
        alasan: `You get number ${luck}, ${luck} is not composite and not prime number`,
      });
    } else if (!prime) {
      res.status(201).json({
        message: "Failed to relase pokemon",
        alasan: `You get number ${luck}, ${luck} is not prime number`,
      });
    } else {
      try {
        const { id } = req.params;
        const poke = await mypokemon.findOne({
          where: { id: id },
        });
        const initialRelease = poke.initial_release;
        const release = poke.release;
        const releaseBefore = poke.release_before;
        if (poke) {
          const checkRelease = (counter) => {
            if (counter == 0) {
              const newRelease = release;
              const newReleaseBefore = releaseBefore;
              const newInitialRelease = counter + 1;
              return { newRelease, newReleaseBefore, newInitialRelease };
            } else if (counter == 1) {
              const newRelease = release + 1;
              const newReleaseBefore = releaseBefore;
              const newInitialRelease = counter + 1;
              return { newRelease, newReleaseBefore, newInitialRelease };
            } else {
              const newRelease = release + releaseBefore;
              const newReleaseBefore = release;
              const newInitialRelease = counter + 1;
              return { newRelease, newReleaseBefore, newInitialRelease };
            }
          };
          const result = checkRelease(initialRelease);
          console.log(
            result.newRelease,
            result.newReleaseBefore,
            result.newInitialRelease
          );
          const [renamePokemon] = await mypokemon.update(
            {
              release: result.newRelease,
              release_before: result.newReleaseBefore,
              initial_release: result.newInitialRelease,
              is_rename: true,
            },
            {
              where: { id: id },
            }
          );
          if (renamePokemon) {
            const updatedPokemon = await mypokemon.findOne({
              where: { id: id },
            });
            return res.status(202).json({
              message: "Success release pokemon",
              alasan: `You get number ${luck}, ${luck} is prime number`,
              data: updatedPokemon,
            });
          }
        } else {
          return res
            .status(404)
            .send("Pokemon with the specified ID does not exists");
        }
      } catch (error) {
        return res.status(500).send(error.message);
      }
    }
  },
};
