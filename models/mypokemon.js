"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class mypokemon extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  mypokemon.init(
    {
      nickname: DataTypes.STRING,
      pokemon_name: DataTypes.STRING,
      release: DataTypes.INTEGER,
      release_before: DataTypes.INTEGER,
      initial_release: DataTypes.INTEGER,
      is_rename: DataTypes.BOOLEAN,
    },
    {
      sequelize,
      modelName: "mypokemon",
    }
  );
  return mypokemon;
};
