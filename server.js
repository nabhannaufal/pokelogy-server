const express = require("express");
const app = express();
const { PORT = 8000 } = process.env;
const bodyParser = require("body-parser");
const cors = require("cors");

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());

const router = require("./routers/router");
app.use(router);
app.listen(PORT, () => {
  console.log(`Server run in port: ${PORT}`);
});

module.exports = app;
