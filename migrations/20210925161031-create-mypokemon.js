'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('mypokemons', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      nickname: {
        type: Sequelize.STRING
      },
      pokemon_name: {
        type: Sequelize.STRING
      },
      release: {
        type: Sequelize.INTEGER
      },
      release_before: {
        type: Sequelize.INTEGER
      },
      initial_release: {
        type: Sequelize.INTEGER
      },
      is_rename: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('mypokemons');
  }
};