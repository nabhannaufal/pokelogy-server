const router = require("express").Router();
const pokemon = require("../controllers/pokemonController");

//pokemon api
router.get("/", pokemon.index);
router.post("/api/catch", pokemon.catchPokemon);
router.put("/api/rename/:id", pokemon.rename);
router.get("/api/release/:id", pokemon.release);
router.get("/api/mypokemon", pokemon.getPokemon);
router.get("/api/mypokemon/:id", pokemon.getPokemonId);
router.delete("/api/mypokemon/:id", pokemon.deletePokemon);

module.exports = router;
